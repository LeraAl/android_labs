package aleshko.lab_3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

enum Mode { Bin, Dec, Hex}

public class ProgrammingActivity extends Activity implements View.OnClickListener{

    TextView upTextView;
    TextView signTextView;
    TextView downTextView;

    ArrayList<Integer> digitBtns = new ArrayList<>(10);
    ArrayList<Integer> operBtns = new ArrayList<>(4);

    int btnBin;
    int btnDec;
    int btnHex;

    int btnOk;
    int btnDot;
    int btnPM;
    int btnDel;
    int btnBksp;
    boolean hasError;

    Mode mode = Mode.Dec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programming);
        Init();

        Intent intent = getIntent();
        downTextView.setText(intent.getStringExtra("down"));
        hasError = intent.getBooleanExtra("hasError", false);
        upTextView.setText(intent.getStringExtra("up"));
        signTextView.setText(intent.getStringExtra("sign"));
    }

    private void Init(){

        upTextView = (TextView)findViewById(R.id.upText);
        signTextView = (TextView)findViewById(R.id.signText);
        downTextView = (TextView)findViewById(R.id.downText);

        digitBtns.add(R.id.btn0);
        digitBtns.add(R.id.btn1);
        digitBtns.add(R.id.btn2);
        digitBtns.add(R.id.btn3);
        digitBtns.add(R.id.btn4);
        digitBtns.add(R.id.btn5);
        digitBtns.add(R.id.btn6);
        digitBtns.add(R.id.btn7);
        digitBtns.add(R.id.btn8);
        digitBtns.add(R.id.btn9);
        digitBtns.add(R.id.btnA);
        digitBtns.add(R.id.btnB);
        digitBtns.add(R.id.btnC);
        digitBtns.add(R.id.btnD);
        digitBtns.add(R.id.btnE);
        digitBtns.add(R.id.btnF);

        operBtns.add(R.id.btnAND);
        operBtns.add(R.id.btnOR);
        operBtns.add(R.id.btnXOR);
        operBtns.add(R.id.btnNOT);
        operBtns.add(R.id.btnSHL);
        operBtns.add(R.id.btnSHR);

        btnBin = R.id.btnBin;
        btnDec = R.id.btnDec;
        btnHex = R.id.btnHex;

        btnOk = R.id.btnOk;
        btnDel = R.id.btnDel;
        btnBksp = R.id.btnBksp;

        for (int btn : digitBtns)
            findViewById(btn).setOnClickListener(this);
        for (int btn : operBtns)
            findViewById(btn).setOnClickListener(this);

        findViewById(btnBin).setOnClickListener(this);
        findViewById(btnDec).setOnClickListener(this);
        findViewById(btnHex).setOnClickListener(this);
        findViewById(btnOk).setOnClickListener(this);
        findViewById(btnDel).setOnClickListener(this);
        findViewById(btnBksp).setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("up", upTextView.getText().toString());
        outState.putString("down", downTextView.getText().toString());
        outState.putString("sign", signTextView.getText().toString());
        outState.putBoolean("hasError", hasError);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        hasError = savedInstanceState.getBoolean("hasError");
        upTextView.setText(savedInstanceState.getString("up"));
        downTextView.setText(savedInstanceState.getString("down"));
        signTextView.setText(savedInstanceState.getString("sign"));
    }

    @Override
    public void onClick(View view) {
        try {
        if(hasError){
            downTextView.setText("");
            hasError = false;
        }

        if(view.getId() == btnBin){
            ((Button)findViewById(R.id.btnBin)).setBackgroundColor(0xFF4081EE);
            ((Button)findViewById(R.id.btnDec)).setBackgroundColor(0xFFE6E3E4);
            ((Button)findViewById(R.id.btnHex)).setBackgroundColor(0xFFE6E3E4);

            switch(mode.ordinal()){
                case 1:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toBinaryString(Long.parseLong(downTextView.getText().toString())));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toBinaryString(Long.parseLong(upTextView.getText().toString())));
                    break;
                case 2:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toBinaryString(Long.parseLong(downTextView.getText().toString(), 16)));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toBinaryString(Long.parseLong(upTextView.getText().toString(), 16)));
                    break;
            }

            mode = Mode.Bin;
            for (int btn : digitBtns){
                if (btn == R.id.btn0 || btn == R.id.btn1)
                    ((Button)findViewById(btn)).setEnabled(true);
                else
                    ((Button)findViewById(btn)).setEnabled(false);
            }


        }
        else if(view.getId() == btnDec){

            ((Button)findViewById(R.id.btnBin)).setBackgroundColor(0xFFE6E3E4);
            ((Button)findViewById(R.id.btnDec)).setBackgroundColor(0xFF4081EE);
            ((Button)findViewById(R.id.btnHex)).setBackgroundColor(0xFFE6E3E4);

            switch(mode.ordinal()){
                case 0:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toString(Long.parseLong(downTextView.getText().toString(), 2)));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toString(Long.parseLong(upTextView.getText().toString(), 2)));
                    break;
                case 2:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toString(Long.parseLong(downTextView.getText().toString(), 16)));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toString(Long.parseLong(upTextView.getText().toString(), 16)));
                    break;
            }
            mode = Mode.Dec;
            for (int i = 0; i < digitBtns.size(); i++){
                if (i < 10)
                    ((Button)findViewById(digitBtns.get(i))).setEnabled(true);
                else
                    ((Button)findViewById(digitBtns.get(i))).setEnabled(false);
            }
        }
        else if(view.getId() == btnHex){
            ((Button)findViewById(R.id.btnBin)).setBackgroundColor(0xFFE6E3E4);
            ((Button)findViewById(R.id.btnDec)).setBackgroundColor(0xFFE6E3E4);
            ((Button)findViewById(R.id.btnHex)).setBackgroundColor(0xFF4081EE);

            switch(mode.ordinal()){
                case 1:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toHexString(Long.parseLong(downTextView.getText().toString())));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toHexString(Long.parseLong(upTextView.getText().toString())));
                    break;
                case 0:
                    if(!downTextView.getText().toString().isEmpty())
                        downTextView.setText(Long.toHexString(Long.parseLong(downTextView.getText().toString(), 2)));
                    if(!upTextView.getText().toString().isEmpty())
                        upTextView.setText(Long.toHexString(Long.parseLong(upTextView.getText().toString(), 2)));
                    break;
            }
            mode = Mode.Hex;
            for (int btn : digitBtns)
                ((Button)findViewById(btn)).setEnabled(true);
        }


        //region digit
        if(digitBtns.contains(view.getId())){
            String text = (String) downTextView.getText();
            downTextView.setText(text + ((Button)findViewById(view.getId())).getText());
            return;
        }
        //endregion

        //region delete

        if (view.getId() == btnDel){
            downTextView.setText("");
            signTextView.setText("");
            upTextView.setText("");
            return;
        }

        if (view.getId() == btnBksp){
            String text = downTextView.getText().toString();
            if (!text.isEmpty())
                downTextView.setText(text.substring(0, text.length()-1));
            return;
        }

        //endregion

        //region signs, =

        if (operBtns.contains(view.getId())){
            if(upTextView.getText().toString().isEmpty()){
                upTextView.setText(downTextView.getText());
                downTextView.setText("");
            }
            Button button = (Button)findViewById(view.getId());
            signTextView.setText(button.getText());
            return;
        }

        if(view.getId() == R.id.btnNOT){
            if(upTextView.getText().toString().isEmpty()){
                long a = Long.parseLong(upTextView.getText().toString(), 2);
            }
        }

        if (view.getId() == btnOk) {
            if ((upTextView.getText().toString().isEmpty() || downTextView.getText().toString().isEmpty()) && signTextView.getText().toString() == "NOT") {
                downTextView.setText(getResources().getString(R.string.operands_error));
                hasError = true;
                return;
            }
            if (signTextView.getText().toString().isEmpty() || signTextView.getText().toString()
                    == getResources().getString(R.string.operation_error)) {
                signTextView.setText(getResources().getString(R.string.operation_error));
                return;
            }

            long a = 0, b = 0;

            if (mode == Mode.Bin) {
                a = Long.parseLong(upTextView.getText().toString(), 2);
                b = Long.parseLong(downTextView.getText().toString(), 2);
            } else if (mode == Mode.Dec) {
                a = Long.parseLong(upTextView.getText().toString());
                b = Long.parseLong(downTextView.getText().toString());
            } else if (mode == Mode.Hex) {
                a = Long.parseLong(upTextView.getText().toString(), 16);
                b = Long.parseLong(downTextView.getText().toString(), 16);
            }

            long result = 0;
            switch (signTextView.getText().toString()) {
                case "OR":
                    result = a | b;
                    break;
                case "XOR":
                    result = a ^ b;
                    break;
                case "NOT":
                    result = ~a;
                    break;
                case "SHL":
                    result = a << b;
                    break;
                case "SHR":
                    result = a >> b;
                    break;
            }
            if (mode == Mode.Bin) {
                upTextView.setText(Long.toBinaryString(result));
            } else if (mode == Mode.Dec) {
                upTextView.setText(Long.toString(result));
            } else if (mode == Mode.Hex) {
                upTextView.setText(Long.toHexString(result));
            }

            downTextView.setText("");
            signTextView.setText("");

            return;
        }

        }
        catch(Exception ex){}
        //endregion

    }
}

