package aleshko.lab5_adapter;

public class ListItem {
    private String name;
    private int imageResource;

    public ListItem(String name, int image){

        this.name=name;
        this.imageResource = image;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImageResource() {
        return this.imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }
}
