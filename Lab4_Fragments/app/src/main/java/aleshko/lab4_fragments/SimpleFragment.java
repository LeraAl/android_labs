package aleshko.lab4_fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class SimpleFragment extends Fragment implements View.OnClickListener {

    private ICalculatorActivity activity;
    
    ArrayList<Integer> digitBtns = new ArrayList<>(10);
    ArrayList<Integer> operBtns = new ArrayList<>(4);
    int btnOk;
    int btnDot;
    int btnPM;
    int btnDel;
    int btnBksp;
    boolean hasError;

    public SimpleFragment() {
    }

    interface OnFragmentInteractionListener {
        void onFragmentInteraction(String link, int fragmentId);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_simple, container, false);
        Init(view);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activity = (ICalculatorActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                        + " должен реализовывать интерфейс OnFragmentInteractionListener");
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        String down = this.activity.getDownTextView().getText().toString();
    }

    static String getTitle(Context context) {
        return "Simple";
    }

    private void Init(View view) {
        digitBtns.add(R.id.btn0);
        digitBtns.add(R.id.btn1);
        digitBtns.add(R.id.btn2);
        digitBtns.add(R.id.btn3);
        digitBtns.add(R.id.btn4);
        digitBtns.add(R.id.btn5);
        digitBtns.add(R.id.btn6);
        digitBtns.add(R.id.btn7);
        digitBtns.add(R.id.btn8);
        digitBtns.add(R.id.btn9);

        operBtns.add(R.id.btnPlus);
        operBtns.add(R.id.btnMinus);
        operBtns.add(R.id.btnMul);
        operBtns.add(R.id.btnDiv);
        btnOk = R.id.btnOk;
        btnDot = R.id.btnDot;
        btnDel = R.id.btnDel;
        btnBksp = R.id.btnBksp;
        btnPM = R.id.btnPM;

        for (int btn : digitBtns)
            view.findViewById(btn).setOnClickListener(this);
        for (int btn : operBtns)
            view.findViewById(btn).setOnClickListener(this);

        view.findViewById(btnOk).setOnClickListener(this);
        view.findViewById(btnDot).setOnClickListener(this);
        view.findViewById(btnDel).setOnClickListener(this);
        view.findViewById(btnBksp).setOnClickListener(this);
        view.findViewById(btnPM).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (hasError) {
            this.activity.getDownTextView().setText("");
            hasError = false;
        }

        //region digit, dot
        if (digitBtns.contains(view.getId())) {
            String text = (String) this.activity.getDownTextView().getText();
            if (view.getId() == R.id.btn0) {
                if (text.isEmpty())
                    this.activity.getDownTextView().setText(this.activity.getDownTextView().getText().toString() + "0.");
                else
                    this.activity.getDownTextView().setText(text + "0");
            } else
                this.activity.getDownTextView().setText(text + ((Button) view.findViewById(view.getId())).getText());
        }

        if (view.getId() == btnDot) {
            String text = this.activity.getDownTextView().getText().toString();
            if (!text.contains("."))
                if (text.isEmpty())
                    this.activity.getDownTextView().setText("0.");
                else
                    this.activity.getDownTextView().setText(String.format("%s.", text));
        }
        //endregion

        //region delete, +/-

        if (view.getId() == btnPM) {
            String text = (String) this.activity.getDownTextView().getText();
            if (text.startsWith("-"))
                this.activity.getDownTextView().setText(text.substring(1));
            else
                this.activity.getDownTextView().setText(String.format("-%s", text));
        }

        if (view.getId() == btnDel) {
            this.activity.getDownTextView().setText("");
            this.activity.getSignTextView().setText("");
            this.activity.getUpTextView().setText("");
        }

        if (view.getId() == btnBksp) {
            String text = this.activity.getDownTextView().getText().toString();
            if (!text.isEmpty())
                this.activity.getDownTextView().setText(text.substring(0, text.length() - 1));
        }

        //endregion

        //region signs, =

        if (operBtns.contains(view.getId())) {
            if (this.activity.getUpTextView().getText().toString().isEmpty()) {
                this.activity.getUpTextView().setText(this.activity.getDownTextView().getText());
                this.activity.getDownTextView().setText("");
            }
            Button button = (Button) view.findViewById(view.getId());
            this.activity.getSignTextView().setText(button.getText());
        }

        if (view.getId() == btnOk) {
            handleBtnOk();
        }
    }

    private void handleBtnOk(){
        if (this.activity.getUpTextView().getText().toString().isEmpty() || this.activity.getDownTextView().getText().toString().isEmpty()) {
            this.activity.getDownTextView().setText("Not enough operands.");
            hasError = true;
            return;
        }
        if (this.activity.getSignTextView().getText().toString().isEmpty() || this.activity.getSignTextView().getText().toString() == "Choose operation") {
            this.activity.getSignTextView().setText("Choose operation");
            return;
        }
        double a = Double.parseDouble(this.activity.getUpTextView().getText().toString());
        double b = Double.parseDouble(this.activity.getDownTextView().getText().toString());

        if (b == 0) {
            this.activity.getDownTextView().setText("Eror. Zero division.");
            hasError = true;
            return;
        }

        double result = 0;
        switch (this.activity.getSignTextView().getText().toString()) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
        }
        this.activity.getUpTextView().setText(String.valueOf(result));
        this.activity.getDownTextView().setText("");
        this.activity.getSignTextView().setText("");
    }
}