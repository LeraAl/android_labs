package aleshko.lab4_fragments;

import android.widget.TextView;

/**
 * Created by Aleshko on 16.11.2017.
 */

interface ICalculatorActivity {
    public TextView getUpTextView();
    public TextView getSignTextView();
    public TextView getDownTextView();
}
