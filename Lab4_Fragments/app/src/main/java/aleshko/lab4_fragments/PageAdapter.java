package aleshko.lab4_fragments;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

class PageAdapter extends FragmentStatePagerAdapter {

    private Context context = null;

    ArrayList<Fragment> fragments;

    PageAdapter(Context context, FragmentManager mgr) {
        super(mgr);
        this.context = context;
        fragments = new ArrayList<>(2);
        fragments.add(0, new SimpleFragment());
        fragments.add(1, new EngineeringFragment());
    }

    @Override
    public int getCount() {
        return(2);
    }

    @Override
    public Fragment getItem(int position) {
        return position == 0 ? new SimpleFragment() : new EngineeringFragment();
    }

    @Override
    public String getPageTitle(int position) {
        if(position == 0)
            return SimpleFragment.getTitle(context);
        else
            return EngineeringFragment.getTitle(context);
    }
}
