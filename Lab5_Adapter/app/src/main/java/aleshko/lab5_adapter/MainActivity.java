package aleshko.lab5_adapter;

import android.content.res.TypedArray;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import aleshko.lab5_adapter.ListAdapter;
import aleshko.lab5_adapter.ListItem;
import aleshko.lab5_adapter.R;

public class MainActivity extends AppCompatActivity {

    private ArrayList<ListItem> items = new ArrayList();
    private Integer[] images = new Integer[15];
    int selectedImageSpinner = -1;
    private ListAdapter listAdapter;
    private SpinnerAdapter spinnerAdapter;
    ListView imageList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setInitialData();

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.getLayoutParams().width = 3;
        spinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_row, images);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedImageSpinner = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                selectedImageSpinner = -1;
            }
        });

        listAdapter = new ListAdapter(this, R.layout.list_item, items);
        imageList = (ListView) findViewById(R.id.image_list);
        imageList.setAdapter(listAdapter);
        imageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ListItem selectedState = (ListItem) parent.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), "Был выбран пункт " + selectedState.getName(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setInitialData(){

        items.add(new ListItem ("Image1", R.drawable.i1));
        items.add(new ListItem ("Image2", R.drawable.i2));
        items.add(new ListItem ("Image3", R.drawable.i3));
        items.add(new ListItem ("Image4", R.drawable.i4));
        items.add(new ListItem ("Image5", R.drawable.i5));

        int i = 0;
        images[i++] = R.drawable.i1;
        images[i++] = R.drawable.i2;
        images[i++] = R.drawable.i3;
        images[i++] = R.drawable.i4;
        images[i++] = R.drawable.i5;
        images[i++] = R.drawable.i6;
        images[i++] = R.drawable.i7;
        images[i++] = R.drawable.i8;
        images[i++] = R.drawable.i9;
        images[i++] = R.drawable.i10;
        images[i++] = R.drawable.i11;
        images[i++] = R.drawable.i12;
        images[i++] = R.drawable.i13;
        images[i++] = R.drawable.i14;
        images[i++] = R.drawable.i15;
    }

    public void add(View view){
        if(selectedImageSpinner != -1){
            EditText imageNameEditText = (EditText) findViewById(R.id.image_name);
            ListItem item = new ListItem(imageNameEditText.getText().toString(), (int)spinnerAdapter.getItem(selectedImageSpinner));
            listAdapter.add(item);
            listAdapter.notifyDataSetChanged();
        }

    }
}