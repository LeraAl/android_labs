package aleshko.lab5_adapter;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class SpinnerAdapter extends ArrayAdapter<Integer>
{

    private final Activity context;

    private final Integer[] names;

    public SpinnerAdapter(Activity context, int textViewResourceId, Integer[] objects)
    {
        super(context, R.layout.spinner_row, objects);
        this.context = context;
        this.names = objects;
    }

    static class ViewHolder
    {
        public ImageView imageView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getImageView(position, convertView, parent);
    }

    @Nullable
    @Override
    public Integer getItem(int position) {
        return names[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        return getImageView(position, convertView, parent);
    }

    private View getImageView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;

        View rowView = convertView;

        if (rowView == null)
        {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.spinner_row, null, true);
            holder = new ViewHolder();
            holder.imageView = (ImageView) rowView.findViewById(R.id.icon);
            rowView.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) rowView.getTag();
        }

        Integer integer = names[position];

        holder.imageView.setImageResource(integer);

        return rowView;
    }


}

