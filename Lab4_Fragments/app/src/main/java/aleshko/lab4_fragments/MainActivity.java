package aleshko.lab4_fragments;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ICalculatorActivity {


    TextView upTextView;
    TextView signTextView;
    TextView downTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        upTextView = (TextView)findViewById(R.id.upText);
        signTextView = (TextView)findViewById(R.id.signText);
        downTextView = (TextView)findViewById(R.id.downText);

        ViewPager pager=(ViewPager)findViewById(R.id.pager);
        pager.setAdapter(new PageAdapter(this, getSupportFragmentManager()));
    }

    public TextView getUpTextView() {
        return upTextView;
    }

    public TextView getSignTextView() {
        return signTextView;
    }

    public TextView getDownTextView() {
        return downTextView;
    }

}