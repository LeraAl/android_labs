package aleshko.lab4_fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class EngineeringFragment extends Fragment implements View.OnClickListener{

    private ICalculatorActivity activity;

    ArrayList<Integer> digitBtns = new ArrayList<>(10);
    int btnSin;
    int btnCos;
    int btnTan;
    int btnCtg;
    int btnSqrt;
    int btnPow2;
    int btnPow3;
    int btnDiv;
    int btnLg;
    int btnLn;
    int btnOk;
    int btnDot;
    int btnPM;
    int btnDel;
    int btnBksp;
    boolean hasError;

    public EngineeringFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_engineering, container, false);
        Init(view);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            activity = (ICalculatorActivity) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " должен реализовывать интерфейс ICalculatorActivity");
        }
    }

    static String getTitle(Context context) {
        return "Engineering";
    }

    private void Init(View view) {
        digitBtns.add(R.id.btn0);
        digitBtns.add(R.id.btn1);
        digitBtns.add(R.id.btn2);
        digitBtns.add(R.id.btn3);
        digitBtns.add(R.id.btn4);
        digitBtns.add(R.id.btn5);
        digitBtns.add(R.id.btn6);
        digitBtns.add(R.id.btn7);
        digitBtns.add(R.id.btn8);
        digitBtns.add(R.id.btn9);

        btnSin = R.id.btnSin;
        btnCos = R.id.btnCos;
        btnTan = R.id.btnTan;
        btnCtg = R.id.btnCtg;
        btnSqrt = R.id.btnSqrt;
        btnPow2 = R.id.btnPow2;
        btnPow3 = R.id.btnPow3;
        btnDiv = R.id.btnDiv;
        btnLg = R.id.btnLg;
        btnLn = R.id.btnLn;
        btnOk = R.id.btnOk;
        btnDot = R.id.btnDot;
        btnDel = R.id.btnDel;
        btnBksp = R.id.btnBksp;
        btnPM = R.id.btnPM;

        for (int btn : digitBtns)
            view.findViewById(btn).setOnClickListener(this);

        view.findViewById(btnSin).setOnClickListener(this);
        view.findViewById(btnCos).setOnClickListener(this);
        view.findViewById(btnTan).setOnClickListener(this);
        view.findViewById(btnCtg).setOnClickListener(this);
        view.findViewById(btnSqrt).setOnClickListener(this);
        view.findViewById(btnPow2).setOnClickListener(this);
        view.findViewById(btnPow3).setOnClickListener(this);
        view.findViewById(btnDiv).setOnClickListener(this);
        view.findViewById(btnLg).setOnClickListener(this);
        view.findViewById(btnLn).setOnClickListener(this);

        view.findViewById(btnDot).setOnClickListener(this);
        view.findViewById(btnDel).setOnClickListener(this);
        view.findViewById(btnBksp).setOnClickListener(this);
        view.findViewById(btnPM).setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (hasError) {
            this.activity.setDownText("");
            hasError = false;
        }

        String text = this.activity.getDownTextView().getText().toString();

        //region digit, dot

        if (digitBtns.contains(view.getId())) {
            if (view.getId() == R.id.btn0) {
                if (text.isEmpty())
                    this.activity.getDownTextView().setText(this.activity.getDownTextView().getText().toString() + "0.");
                else
                    this.activity.getDownTextView().setText(text + "0");
            } else
                this.activity.getDownTextView().setText(text + ((Button) view.findViewById(view.getId())).getText());
        }
        else if (view.getId() == btnDot) {
            if (!text.contains("."))
                if (text.isEmpty())
                    this.activity.getDownTextView().setText("0.");
                else
                    this.activity.getDownTextView().setText(String.format("%s.", text));
        }
        else if (view.getId() == btnPM) {
            if (text.startsWith("-"))
                this.activity.getDownTextView().setText(text.substring(1));
            else
                this.activity.getDownTextView().setText(String.format("-%s", text));
        }
        else if (view.getId() == btnBksp) {
            if (!text.isEmpty())
                this.activity.getDownTextView().setText(text.substring(0, text.length() - 1));
        }

        else

        //endregion

        //region delete, +/-


            if (text.isEmpty() || text.equals("-")) {
                this.activity.getDownTextView().setText("Not enough operands");
                hasError = true;
                return;
            }

        //endregion

        //region operations



        try {

            if (view.getId() == btnSin) {
                double number = Double.parseDouble(text);
                number = Math.sin(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }

            else if (view.getId() == btnCos){
                double number = Double.parseDouble(text);
                number = Math.cos(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }

            else if (view.getId() == btnTan){
                double number = Double.parseDouble(text);
                number = Math.tan(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }

            else if (view.getId() == btnCtg){
                double number = Double.parseDouble(text);
                number =1.0 /  Math.tan(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnSqrt){
                double number = Double.parseDouble(text);
                number = Math.sqrt(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnPow2){
                double number = Double.parseDouble(text);
                number = Math.pow(number, 2);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnPow3){
                double number = Double.parseDouble(text);
                number = Math.pow(number, 3);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnLn){
                double number = Double.parseDouble(text);
                number = Math.log(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnLg){
                double number = Double.parseDouble(text);
                number = Math.log10(number);
                this.activity.getDownTextView().setText(String.valueOf(number));
            }
            else if (view.getId() == btnDiv){
                double number = Double.parseDouble(text);
                number = 1.0 / number;
                this.activity.getDownTextView().setText(String.valueOf(number));
            }

            if (view.getId() == btnDel) {
                this.activity.getDownTextView().setText("");
            }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        //endregion

    }
}