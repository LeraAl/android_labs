package aleshko.labs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class ExtraActivity extends AppCompatActivity implements View.OnClickListener {

    //region Buttons & TextView

    String upText, signText;
    TextView numTextView;
    ArrayList<Integer> digitBtns = new ArrayList<>(10);
    int btnSin;
    int btnCos;
    int btnTan;
    int btnCtg;
    int btnSqrt;
    int btnPow2;
    int btnPow3;
    int btnDiv;
    int btnLg;
    int btnLn;
    int btnOk;
    int btnDot;
    int btnPM;
    int btnDel;
    int btnBksp;
    boolean hasError;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra);
        Init();

        Intent intent = getIntent();
        numTextView.setText(intent.getStringExtra("down"));
        hasError = intent.getBooleanExtra("hasError", false);
        upText = intent.getStringExtra("up");
        signText = intent.getStringExtra("sign");
    }

    private void Init() {
        numTextView = (TextView) findViewById(R.id.numberText);

        digitBtns.add(R.id.btn0);
        digitBtns.add(R.id.btn1);
        digitBtns.add(R.id.btn2);
        digitBtns.add(R.id.btn3);
        digitBtns.add(R.id.btn4);
        digitBtns.add(R.id.btn5);
        digitBtns.add(R.id.btn6);
        digitBtns.add(R.id.btn7);
        digitBtns.add(R.id.btn8);
        digitBtns.add(R.id.btn9);

        btnSin = R.id.btnSin;
        btnCos = R.id.btnCos;
        btnTan = R.id.btnTan;
        btnCtg = R.id.btnCtg;
        btnSqrt = R.id.btnSqrt;
        btnPow2 = R.id.btnPow2;
        btnPow3 = R.id.btnPow3;
        btnDiv = R.id.btnDiv;
        btnLg = R.id.btnLg;
        btnLn = R.id.btnLn;
        btnOk = R.id.btnOk;
        btnDot = R.id.btnDot;
        btnDel = R.id.btnDel;
        btnBksp = R.id.btnBksp;
        btnPM = R.id.btnPM;

        for (int btn : digitBtns)
            findViewById(btn).setOnClickListener(this);

        findViewById(btnSin).setOnClickListener(this);
        findViewById(btnCos).setOnClickListener(this);
        findViewById(btnTan).setOnClickListener(this);
        findViewById(btnCtg).setOnClickListener(this);
        findViewById(btnSqrt).setOnClickListener(this);
        findViewById(btnPow2).setOnClickListener(this);
        findViewById(btnPow3).setOnClickListener(this);
        findViewById(btnDiv).setOnClickListener(this);
        findViewById(btnLg).setOnClickListener(this);
        findViewById(btnLn).setOnClickListener(this);

        findViewById(btnDot).setOnClickListener(this);
        findViewById(btnDel).setOnClickListener(this);
        findViewById(btnBksp).setOnClickListener(this);
        findViewById(btnPM).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu item) {
        getMenuInflater().inflate(R.menu.menu_extra, item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.menu_item_main){
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("down", numTextView.getText().toString());
            intent.putExtra("up", upText);
            intent.putExtra("sign", signText);
            intent.putExtra("hasError", hasError);
            startActivity(intent);
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("up", upText);
        outState.putString("down", numTextView.getText().toString());
        outState.putString("sign", signText);
        outState.putBoolean("hasError", hasError);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        hasError = savedInstanceState.getBoolean("hasError");
        upText = savedInstanceState.getString("up");
        signText = savedInstanceState.getString("sign");
        numTextView.setText(savedInstanceState.getString("down"));
    }

    @Override
    public void onClick(View view) {
        if (hasError) {
            numTextView.setText("");
            hasError = false;
        }

        String text = numTextView.getText().toString();

        //region digit, dot

        if (digitBtns.contains(view.getId())) {
            if (view.getId() == R.id.btn0) {
                if (text.isEmpty())
                    numTextView.setText(numTextView.getText().toString() + "0.");
                else
                    numTextView.setText(text + "0");
            } else
                numTextView.setText(text + ((Button) findViewById(view.getId())).getText());
            return;
        }

        if (view.getId() == btnDot) {
            if (!text.contains("."))
                if (text.isEmpty())
                    numTextView.setText("0.");
                else
                    numTextView.setText(String.format("%s.", text));
            return;
        }

        //endregion

        //region delete, +/-

        if (view.getId() == btnPM) {
            if (text.startsWith("-"))
                numTextView.setText(text.substring(1));
            else
                numTextView.setText(String.format("-%s", text));
            return;
        }

        if (view.getId() == btnDel) {
            numTextView.setText("");
            return;
        }

        if (view.getId() == btnBksp) {
            if (!text.isEmpty())
                numTextView.setText(text.substring(0, text.length() - 1));
            return;
        }

        //endregion

        //region operations

        if (text.isEmpty() || text.equals("-")) {
            numTextView.setText("Not enough operands");
            hasError = true;
            return;
        }

        double number = Double.parseDouble(text);
        if (view.getId() == btnSin)
            number = Math.sin(number);
        else if (view.getId() == btnCos)
            number = Math.cos(number);
        else if (view.getId() == btnTan)
            number = Math.tan(number);
        else if (view.getId() == btnCtg)
            number = 1.0 / Math.tan(number);
        else if (view.getId() == btnSqrt)
            number = Math.sqrt(number);
        else if (view.getId() == btnPow2)
            number = Math.pow(number, 2);
        else if (view.getId() == btnPow3)
            number = Math.pow(number, 3);
        else if (view.getId() == btnLn)
            number = Math.log(number);
        else if (view.getId() == btnLg)
            number = Math.log10(number);
        else if (view.getId() == btnDiv)
            number = 1.0 / number;

        numTextView.setText(String.valueOf(number));

        //endregion

    }

}
