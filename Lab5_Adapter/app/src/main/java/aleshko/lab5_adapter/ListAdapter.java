package aleshko.lab5_adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Aleshko on 20.11.2017.
 */

public class ListAdapter extends ArrayAdapter<ListItem> {

    private LayoutInflater inflater;
    private int layout;
    private List<ListItem> states;

    public ListAdapter(Context context, int resource, List<ListItem> states) {
        super(context, resource, states);
        this.states = states;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }
    public View getView(int position, View convertView, ViewGroup parent) {

        View view=inflater.inflate(this.layout, parent, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.image);
        TextView nameView = (TextView) view.findViewById(R.id.name);

        ListItem item = states.get(position);

        imageView.setImageResource(item.getImageResource());
        nameView.setText(item.getName());

        return view;
    }
}
