package aleshko.lab_3;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

public class MainActivity extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost tabHost = getTabHost();

        TabHost.TabSpec tabSpec;

        tabSpec = tabHost.newTabSpec("simple");
        tabSpec.setIndicator(getResources().getString(R.string.simple));
        tabSpec.setContent(new Intent(this, SimpleActivity.class));
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("programming");
        tabSpec.setIndicator(getResources().getString(R.string.programming));
        tabSpec.setContent(new Intent(this, ProgrammingActivity.class));
        tabHost.addTab(tabSpec);
    }
}
