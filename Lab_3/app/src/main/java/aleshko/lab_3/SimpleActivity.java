package aleshko.lab_3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SimpleActivity extends AppCompatActivity implements View.OnClickListener{

    TextView upTextView;
    TextView signTextView;
    TextView downTextView;

    ArrayList<Integer> digitBtns = new ArrayList<>(10);
    ArrayList<Integer> operBtns = new ArrayList<>(4);
    int btnOk;
    int btnDot;
    int btnPM;
    int btnDel;
    int btnBksp;
    boolean hasError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple);
        Init();

        Intent intent = getIntent();
        downTextView.setText(intent.getStringExtra("down"));
        hasError = intent.getBooleanExtra("hasError", false);
        upTextView.setText(intent.getStringExtra("up"));
        signTextView.setText(intent.getStringExtra("sign"));
    }

    private void Init(){

        upTextView = (TextView)findViewById(R.id.upText);
        signTextView = (TextView)findViewById(R.id.signText);
        downTextView = (TextView)findViewById(R.id.downText);

        digitBtns.add(R.id.btn0);
        digitBtns.add(R.id.btn1);
        digitBtns.add(R.id.btn2);
        digitBtns.add(R.id.btn3);
        digitBtns.add(R.id.btn4);
        digitBtns.add(R.id.btn5);
        digitBtns.add(R.id.btn6);
        digitBtns.add(R.id.btn7);
        digitBtns.add(R.id.btn8);
        digitBtns.add(R.id.btn9);

        operBtns.add(R.id.btnPlus);
        operBtns.add(R.id.btnMinus);
        operBtns.add(R.id.btnMul);
        operBtns.add(R.id.btnDiv);
        btnOk = R.id.btnOk;
        btnDot = R.id.btnDot;
        btnDel = R.id.btnDel;
        btnBksp = R.id.btnBksp;
        btnPM = R.id.btnPM;

        for (int btn : digitBtns)
            findViewById(btn).setOnClickListener(this);
        for (int btn : operBtns)
            findViewById(btn).setOnClickListener(this);

        findViewById(btnOk).setOnClickListener(this);
        findViewById(btnDot).setOnClickListener(this);
        findViewById(btnDel).setOnClickListener(this);
        findViewById(btnBksp).setOnClickListener(this);
        findViewById(btnPM).setOnClickListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("up", upTextView.getText().toString());
        outState.putString("down", downTextView.getText().toString());
        outState.putString("sign", signTextView.getText().toString());
        outState.putBoolean("hasError", hasError);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        hasError = savedInstanceState.getBoolean("hasError");
        upTextView.setText(savedInstanceState.getString("up"));
        downTextView.setText(savedInstanceState.getString("down"));
        signTextView.setText(savedInstanceState.getString("sign"));
    }

    @Override
    public void onClick(View view) {
        if(hasError){
            downTextView.setText("");
            hasError = false;
        }

        //region digit, dot
        if(digitBtns.contains(view.getId())){
            String text = (String) downTextView.getText();
            if(view.getId() == R.id.btn0) {
                if (text.isEmpty())
                    downTextView.setText(downTextView.getText().toString() + "0.");
                else
                    downTextView.setText(text + "0");
            }
            else
                downTextView.setText(text + ((Button)findViewById(view.getId())).getText());
            return;
        }

        if(view.getId() == btnDot){
            String text = downTextView.getText().toString();
            if (!text.contains("."))
                if(text.isEmpty())
                    downTextView.setText("0.");
                else
                    downTextView.setText(String.format("%s.", text));
            return;
        }
        //endregion

        //region delete, +/-

        if(view.getId() == btnPM){
            String text = (String) downTextView.getText();
            if (text.startsWith("-"))
                downTextView.setText(text.substring(1));
            else
                downTextView.setText(String.format("-%s", text));
            return;
        }

        if (view.getId() == btnDel){
            downTextView.setText("");
            signTextView.setText("");
            upTextView.setText("");
            return;
        }

        if (view.getId() == btnBksp){
            String text = downTextView.getText().toString();
            if (!text.isEmpty())
                downTextView.setText(text.substring(0, text.length()-1));
            return;
        }

        //endregion

        //region signs, =

        if (operBtns.contains(view.getId())){
            if(upTextView.getText().toString().isEmpty()){
                upTextView.setText(downTextView.getText());
                downTextView.setText("");
            }
            Button button = (Button)findViewById(view.getId());
            signTextView.setText(button.getText());
            return;
        }

        if (view.getId() == btnOk){
            if(upTextView.getText().toString().isEmpty() || downTextView.getText().toString().isEmpty()){
                downTextView.setText(getResources().getString(R.string.operands_error));
                hasError = true;
                return;
            }
            if(signTextView.getText().toString().isEmpty() || signTextView.getText().toString() == getResources().getString(R.string.operation_error)){
                signTextView.setText(getResources().getString(R.string.operation_error));
                return;
            }
            double a = Double.parseDouble(upTextView.getText().toString());
            double b = Double.parseDouble(downTextView.getText().toString());

            if(b == 0){
                downTextView.setText(getResources().getString(R.string.zero_error));
                hasError = true;
                return;
            }


            double result = 0;
            switch (signTextView.getText().toString()){
                case "+":
                    result = a + b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "/":
                    result = a / b;
                    break;
            }
            upTextView.setText(String.valueOf(result));
            downTextView.setText("");
            signTextView.setText("");

            return;
        }

        //endregion

    }
}